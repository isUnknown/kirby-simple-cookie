<?php
$message = $message ?? 'Ce site utilise des cookies pour améliorer votre expérience de navigation et réaliser des statistiques de visites.';
?>

<link rel="stylesheet" href="<?= url('media/plugins/adrienpayet/kirby-simple-cookie/style.css') ?>">
<script src="<?= url('media/plugins/adrienpayet/kirby-simple-cookie/index.js') ?>"></script>
<div class="simple-cookie-banner hidden">
  <p class="simple-cookie-banner__message"><?= $message ?></p>
  <div class="simple-cookie-banner__buttons">
    <button class="simple-cookie-banner__accept">accepter</button>
    <button class="simple-cookie-banner__deny">refuser</button>
  </div>
</div>