<?php

Kirby::plugin('adrienpayet/kirby-simple-cookie', [
  'snippets' => [
    'cookie-banner' => __DIR__ . '/snippets/cookie-banner.php',
    'cookie-scripts' => __DIR__ . '/snippets/cookie-scripts.php'
  ],
]);