document.addEventListener("DOMContentLoaded", () => {
  if (!sessionStorage.getItem("cookieAccepted")) {
    document.querySelector(".simple-cookie-banner").classList.remove("hidden");
  }
  document
    .querySelector(".simple-cookie-banner__accept")
    .addEventListener("click", () => {
      sessionStorage.setItem("cookieAccepted", true);
      window.location.reload();
    });
  document
    .querySelector(".simple-cookie-banner__deny")
    .addEventListener("click", () => {
      sessionStorage.removeItem("cookieAccepted");
      window.location.reload();
    });
});
