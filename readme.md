# Kirby simple cookie plugin

A simple cookie banner and snippet that allows to add internal and external scripts based on a session data.

## Install

```bash
composer require adrienpayet/kirby-simple-cookie
```

## Usage

### Banner

Add the `<?php snippet('cookie-banner') ?>` inside the `<footer>` tag (or anywhere else inside the `<body>`).

### Scripts

The plugin supports internal and external scripts. Both can be multiple, are optionnal and will be executed only if cookies are accepted.
Internal scripts are added through the snippet 'internal' variable. External scripts are added through the "external" slot.

Example :

```php
<?php snippet('cookie-scripts',
  [
    "internal" => "
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'XX-XXXXXXXXX');
    "
  ]
) ?>
  <? slot('external') ?>
    <script async src="https://www.googletagmanager.com/gtag/js?id=XX-XXXXXXXXX"></script>
  <? endslot() ?>
<?php endsnippet() ?>
```
